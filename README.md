# ssh-knock

ping like utility for SSH

```
$ bash ssh-knock.sh codeberg.org
SSH-KNOCK codeberg.org (codeberg.org)
Connection to codeberg.org (codeberg.org) port 22 [tcp/ssh] established: time=133.75 ms

--- codeberg.org knock statistics ---
1 knocks, 1 successful, 0% loss

```
