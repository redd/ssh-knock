#!/bin/bash

if [ -z "$1" ]
then
  echo "Missing parameter!"
  exit -1
fi

HOSTNAME=`ssh -G $1 | grep hostname | head -n 1 | cut -d " " -f 2`
TIMEOUT=1  # seconds
SLEEP=1 # sleep time between knocks

echo SSH-KNOCK $1 \($HOSTNAME\)
#echo | tr -d "\d"

#host $HOSTNAME > /dev/null 
#if [ $? -eq 1 ]
#then
#  echo Failed to resolve $HOSTNAME
#  exit -2
#fi

COUNT=0
COUNT_SUCCESS=0
TIMES=4
EXIT=1

while [ $COUNT -lt $TIMES ]
do 
  if [ $COUNT -gt 0 ]; then sleep $SLEEP; fi

  COUNT=`expr $COUNT + 1`
  
  # save timestamp for absolute time measurement
  TIMESTAMP1=`date +%s%N`

  # netcat options:
  #   -w : timeout)
  #   -z : knock on port, without sending any data
  #   -v : verbose output
  nc -w $TIMEOUT -z -v $HOSTNAME 22 &> /dev/null; 

  EXIT=$?
  TIMESTAMP2=`date +%s%N`

  if [ ! $EXIT -eq 0 ]; 
  then true; # echo failed; 
  else 
    echo "Connection to $1 ($HOSTNAME) port 22 [tcp/ssh] established: time=" | tr -d "\n"
    echo "scale=2; ($TIMESTAMP2 - $TIMESTAMP1) / 1000000.0" | bc | tr -d "\n"
    echo " ms"
    COUNT_SUCCESS=`expr $COUNT_SUCCESS + 1`;
  fi

  #echo $TIMESTAMP
  #DIFFTIME=`expr $TIMESTAMP2 - $TIMESTAMP1`
  # $echo "$DIFFTIME / 1000000" | bc 

done

echo 
echo --- $1 knock statistics --- 
RATE_LOSS=`echo "scale=0; 100 - 100 * $COUNT_SUCCESS / $COUNT" | bc`
echo $COUNT knocks, $COUNT_SUCCESS successful, $RATE_LOSS% loss
echo

exit $EXIT
